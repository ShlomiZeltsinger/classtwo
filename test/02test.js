const LibInterFace = artifacts.require("LibInterFace");
const LibOne = artifacts.require("LibOne");
const LibTwo = artifacts.require("LibTwo");
const Dispatcher = artifacts.require("Dispatcher");
const SimpleContract = artifacts.require("SimpleContract");
const DispatcherStorage = artifacts.require("DispatcherStorage");

expect = require("chai").expect;

contract("02test file", function(){
	describe("Deploys and grab LibTwo, LibTwo and DispatcherStorage",function(){
		it("Should deploy and grab LibOne", function(){
			return LibOne.new().then(res=>{
				libOne = res;
			});
		});
		it("Should deploy and grab LibTwo", function(){
			return LibTwo.new().then(res=>{
				libTwo = res;
			});
		});
		it("Should Deploy and grab DispatcherStorage - connected to LibOne", function(){
			return DispatcherStorage.new(libOne.address).then(res=>{
				dispatcherStorage = res;
			});
		});
	});

	describe("Deploys and grab the Dispatcher and the SimpleContract", function(){
		it("Deploys and grab the Dispatcher", function(){
			Dispatcher.unlinked_binary = Dispatcher.unlinked_binary.replace("1111222233334444555566667777888899990000", dispatcherStorage.address.slice(2));
			return Dispatcher.new().then(res=>{
				dispatcher = res;
			});
		});
		it("Connect SimpleContract to the LibInterFace but gives it the dispatcher address. Then it deploys and grabs the SimpleContract", function(){
			SimpleContract.link("LibInterFace", dispatcher.address);

			return SimpleContract.new().then(res=>{
				simpleContract = res;
			});
		});
	});

	describe("Changing the different libs", function(){
		it("Use simpleContract with libOne", function(){
			return simpleContract.replaceTheNum(5).then(()=>{
				return simpleContract.getNum().then(res=>{
					expect(res.toNumber()).to.be.equal(5);
				});
			});
		});
		it("Should replace the lib from LibOne to LibTwo", function(){
			return dispatcherStorage.replace(libTwo.address);
		});

		it("Use simpleContract with libTwo", function(){
			return simpleContract.replaceTheNum(3).then(()=>{
				return simpleContract.getNum().then(res=>{
					expect(res.toNumber()).to.be.equal(6);
				});
			});
		});	

	});


});