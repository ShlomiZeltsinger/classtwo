const LibInterFace = artifacts.require("LibInterFace");
const LibOne = artifacts.require("LibOne");
const LibTwo = artifacts.require("LibTwo");
const SimpleContract = artifacts.require("SimpleContract");

expect = require("chai").expect;

contract("01test file", function(){
	describe("Checking for LibOne",function(){
		it("Should deploy and grab LibOne", function(){
			return LibOne.new().then(res=>{
				libOne = res;
			});
		});
		it("Should link and deploy SimpleContract with LibOne", function(){
			SimpleContract.link("LibInterFace", libOne.address);

			return SimpleContract.new().then(res=>{
				simpleContract = res;
			});
		});
		it("Should changeNum to five, the stored value should be five", function(){
			return simpleContract.replaceTheNum(5).then(()=>{
				return simpleContract.getNum().then(res=>{
					expect(res.toNumber()).to.be.equal(5);
				});
			});
		});
	});
	describe("Checking for LibTow",function(){
		it("Should deploy and grab LibTwo", function(){
			return LibTwo.new().then(res=>{
				libTwo = res;
			});
		});
		it("Should link and deploy SimpleContract with LibTwo", function(){
			SimpleContract.link("LibInterFace", libTwo.address);

			return SimpleContract.new().then(res=>{
				simpleContract = res;
			});
		});
		it("Should changeNum to five, the stored value should be Ten", function(){
			return simpleContract.replaceTheNum(5).then(()=>{
				return simpleContract.getNum().then(res=>{
					expect(res.toNumber()).to.be.equal(10);
				});
			});
		});
	});


});