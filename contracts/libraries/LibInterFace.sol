pragma solidity ^0.4.11;

library LibInterFace{
	
	struct Data{
		uint num;
	}

	function changeNum(Data storage data, uint _num) public;
}