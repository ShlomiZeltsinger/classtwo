pragma solidity ^0.4.11;


contract DispatcherStorage{
	address public lib;
	mapping(bytes4=>uint32) public sizes;

	function DispatcherStorage(address _libAddress){
		sizes[bytes4(0xa6a9f60c)] = 32;
		replace(_libAddress);
	}


	function replace(address _newLibAddress) public returns (bool){
		lib = _newLibAddress;
		return true;
	}
}