pragma solidity ^0.4.11;

import "./LibInterFace.sol";

library LibOne{
	
	function changeNum(LibInterFace.Data storage data, uint _num) public{
		data.num = _num;
	}
}