pragma solidity ^0.4.11;

import "./libraries/LibInterFace.sol";

contract SimpleContract{
	
	LibInterFace.Data localData;

	function replaceTheNum(uint _num) public returns (bool){
		LibInterFace.changeNum(localData, _num);
		return true;
	}

	function getNum() public constant returns (uint){
		return localData.num;
	}

}