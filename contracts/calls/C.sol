pragma solidity ^0.4.11;

import "./D.sol";

contract C {
    function foo(D _d, address _e, uint _n) {
        _d.delegatecallSetN(_e, _n); 
        // C just calls D
        // It's not a delegateCall from C to D
    }
}