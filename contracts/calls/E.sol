pragma solidity ^0.4.11;


contract E {
  uint public n;
  address public sender;

  function setN(uint _n) {
    n = _n + n;
    sender = msg.sender;

  }
}




    // msg.sender is D if invoked by D's callcodeSetN. None of E's storage is updated
    // msg.sender is C if invoked by C.foo(). None of E's storage is updated

    // the value of "this" is D, when invoked by either D's callcodeSetN or C.foo()