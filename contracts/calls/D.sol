pragma solidity ^0.4.11;

contract D {
  uint public n;
  address public sender;

    address public _e = 0x2125cfaf583dab78c75bf65d489137ba48081039;

  function callSetN(uint _n) public returns (bool) {
    _e.call(bytes4(sha3("setN(uint256)")), _n); 
    // E's storage is set, D is not modified 
    return true;
  }

  function callcodeSetN(uint _n) public returns (bool) {
    _e.callcode(bytes4(sha3("setN(uint256)")), _n); 
    // D's storage is set, E is not modified (contract address)
    return true;
  }

  function delegatecallSetN(uint _n) public returns (bool){
    _e.delegatecall(bytes4(sha3("setN(uint256)")), _n); 
    // D's storage is set, E is not modified (EOA address)
    return true;
  }
    
  function getThis() constant returns (address){
      return this;
  }
}
